"use strict"

// Get the libs
let chat = require('discord.io'),
	Fb = require("firebase"),
	shot = require("webshot"),
	fs = require('fs'),
	gm = require('gm').subClass({ imageMagick: true }),
	wc = require('node-wolfram'),
	steam = require('steam-webapi'),
	jf = require('jsonfile');

const commands = [
	"!verify",
	"!alpha",
	"!matches",
	"!games",
	"!ready",
	"!unready",
	"!controls",
	"!stream",
	"!signup",
	"!bugs",
	"!wiki",
	"!tournament",
	"!quote",
	"!addquote",
	"!delquote",
	"!getquote",
	"!randquote",
	"!help",
	"!team",
	"!vs",
	"!challenge",
	"!rank",
	"!emotes"
];

const chan = "99240110486192128", // PWG Server/Channel ID?
	TOKEN = process.env.TOKEN,
	WOLFID = process.env.WOLFID,
	FBKEY = process.env.FBKEY;
	//process.env.STEAMKEY;

let players = [], // Whoever is ready
	users = [], // I forgot
	challengers = [], // for King battles
	rdyPpl = players.length,
	conn = 0, // Connection status
	command = [], // Array of people who last used command
	spammer = {}, // Object list of people banned for spam
	mute = false, // Mute? :P
	dance = '',
	wolf = new wc(WOLFID),
	me = "98419293514919936",
	book = '176530812576071680',
	bookDelete = false,
	mastabot = '171782210800582656',
	rules = '195960200237285386',
	noob = '195983997413752832',
	member = '99502270579740672',
	king = '213077157038129154',
	lfg = '238808560106995712',
	history = '196362695367196672',
	playground = '172429393501749248',
	emotes = [
		':rekt:', ':yomama:', ':facefeel:',
		':nerfit:', ':trust:', ':fixit:', ':lol:',
		":wenwut:",":wenclap:", ":wenyes:", ":wenno:", ":wenmore:",":wencheer:",
		":dexclap:",":dexdance:",":dexlittle:",":dexofc:",":dexwut:",
		":schatzeh:",":schatzdance:",":schatzdrop:",":schatzmeteor:"
	],
	u = JSON.parse(fs.readFileSync('assets/units.json', 'utf8')),
	quotes = JSON.parse(fs.readFileSync('assets/quotes.json', 'utf8'));

// Init DB
let config = {
	apiKey: FBKEY,
	authDomain: "ltf-alpha-keys.firebaseapp.com",
	databaseURL: "https://ltf-alpha-keys.firebaseio.com",
	storageBucket: "ltf-alpha-keys.appspot.com",
	serviceAccount: "ltf.json"
 };

Fb.initializeApp(config);

//let quotes = Fb.database().ref("quote");
let soldiers = Fb.database().ref("players"),
	scrims = Fb.database().ref("battles"),
	ranks = Fb.database().ref("ranks"),
	emails = Fb.database().ref("email"),
	keys = Fb.database().ref("key");

// Create the bot name
let bot = new chat.Client({ token: TOKEN, autorun: true });

// Simplify command
function botSay(t,c=chan) {
	bot.sendMessage({ to:c, message: t });
}

function botSend(t,user,c=chan) {
	let emoji = t.replace(/:/g, "");
	let m = (user) ? '`@'+user+':`': null;
	if (emoji.startsWith('emoji/wen') || emoji.startsWith('emoji/dex') || emoji.startsWith('emoji/schatz')) {
		console.log('Uploading '+emoji+'.gif');
		bot.uploadFile({
			to: c,
			file: emoji + '.gif',
			filename: emoji + '.gif',
			message: m
		});
	} else {
		console.log('Uploading '+emoji+'.png');
		bot.uploadFile({
			to: c,
			file: emoji + '.png',
			filename: emoji + '.png',
			message: m
		});
	}
}

function botDel(m,c=chan,t=0) {
	setTimeout( function() {
		bot.deleteMessage({
			channelID: c,
			messageID: m
		});
	}, t);
}

function botEdit(m,t,c=chan) {
	bot.editMessage({
		channelID: c,
		messageID: m,
		message: t
	});
}

// Count function
function getCount(arr, val) {
	let i, j, count = 0;
	for (i=0,j=arr.length;i<j;i++) {
		(arr[i] === val) && count++;
	}
	return count;
}

// Command timer
function popCommand() {
	if (command.length>0)  command.shift();
	//console.log(command);
	setTimeout( function() {
		popCommand();
	},4000);
}

// Get player ID
function getUser(txt) {
	if (txt.match(/\b\d{10,}\b/g)) {
		return txt.match(/\b\d{10,}\b/g)[0];
	} else {
		return undefined;
	}
}

// Player into DB
function regPlayer(from,team="",c=chan) {
	soldiers.once("value", function(snap) {
		let player = soldiers.child(from);
		let x = snap.child(from).exists();
		team = team.replace("red","orange").replace("pink","wine").replace("purple","wine");

		if (!x) {
			// Add and update faction
			player.set( bot.servers[c].members[from] );
			if (team != "") player.update({ faction: team });
		} else {
			// Update faction
			if (team != "") player.update({ faction: team });
		}
	});
}

// Countdown message
function countdownMessage(m,t,c,n) {
	if (n > 0) {
		console.log('Counting Down', n, m);
		setTimeout(function() {
			if (t.includes("🕗")) { // 8 to 10
				botEdit(m,t.replace("🕗","🕙"),c);
				countdownMessage(m,t.replace("🕗","🕙"),c,n-1);
			}
			else if (t.includes("🕕")) { // 6 to 8
				botEdit(m,t.replace("🕕","🕗"),c);
				countdownMessage(m,t.replace("🕕","🕗"),c,n-1);
			}
			else if (t.includes("🕓")) { // 4 to 6
				botEdit(m,t.replace("🕓","🕕"),c);
				countdownMessage(m,t.replace("🕓","🕕"),c,n-1);
			}
			else if (t.includes("🕑")) { // 2 to 4
				botEdit(m,t.replace("🕑","🕓"),c);
				countdownMessage(m,t.replace("🕑","🕓"),c,n-1);
			} else { //1 0 to 12
				botEdit(m,t.replace("🕙","💥"),c);
				countdownMessage(m,t.replace("🕙","💥"),c,0);
			}
		}, 2000);
	} else {
		console.log('Deleting.');
		botDel(m,c,1000);
	}
}

function getEmoji(chan,text,from,id) {
	switch(text) {
		case ":yourmother:":
			text = ':yomama:'
			break;
		case ":patch17:":
			text = ':schatzmeteor:'
			break;
	}

	if (emotes.includes(text) === true) {
		botDel(id,chan);
		botSend('emoji/'+text,from,chan);
	}
}

console.log('Connecting...');

// When bot has connected
bot.on('ready', function() {
	console.log(`${bot.username} - (${bot.id}) ${bot.presenceStatus}`);
	conn = 1;
	popCommand(); // start the command popper
	//soldiers.set(bot.servers[chan].members);
});

// Reconnect bot
bot.on('disconnect', function() {
	conn = 0;
	let tries = 0;

	function reconnect() {
		if (conn===1) return false;

		tries++;
		setTimeout(function() {
			console.log(`Disconnected. Attempting Reconnect #${tries+1}...`);
			bot.connect()
			reconnect(tries)
		},5000*tries);
	}

	if (tries<5 && conn === 0) {
		reconnect(tries);
	} else {
		console.log('Attempted 5 reconnects and failed. Whatevs man.');
	}
});

// Listen for presence changes
bot.on("presence", function(from, fromID, status, game, e) {
	// Someone went offline
	if (status === "offline") {
		console.log(`${from} went offline`);
		// Check if they are on ready list
		if (players.includes(`${fromID}`)) {
			botSay(`Removing ${from} from ready list due to disconnect.`);
			let rem = players.indexOf(`${fromID}`);
			players.splice(rem, 1);
			rdyPpl = players.length;
			console.log(`${from} has been removed`);
		}
	}

	if (status === "online") {
		if ( bot.servers[chan].members[fromID].roles.length === 0 ) {
			// Stuff to tell new person
			let v = [
				`Welcome to the Pocketwatch chat, <@${fromID}>`,
				`Ahoy <@${fromID}>, welcome!`,
				`Glad to have you here, <@${fromID}>`,
				`What's up <@${fromID}>, welcome to the chat!`
			];

			let n = Math.floor( Math.random()*4 );
			botSay(v[n]);
			bot.addToRole({
				serverID: chan,
				userID: fromID,
				roleID: noob
			});

			botSay(`Glad you found the Pocketwatch community, we hope you enjoy your stay. :) \n
Please checkout the <#${rules}> channel for some basic community rules and info on how to get into the alpha. \n
The developers hang out in the chat all the time, so feel free to say hi, ask questions, and tune in to our streams on Fridays @ 5PM EST! \n
For a list of my commands, feel free to type \`!help\` in any channel or in a private message. :thumbsup:`,fromID);
		}
	}

	if (game != null && game.type === 1) {
		botSay(`:movie_camera: <@${fromID}> is streaming! \n <${game.url}>!`)
	}
});

// // Listen for any message
bot.on("message", function(from, fromID, to, txt, raw) {
	let msgID = raw.d.id,
		text = txt.toLowerCase(),
		fromAdmin = (bot.servers[chan].members[fromID].roles.includes('103552189221326848')) ? true : false,
		fromMod = (bot.servers[chan].members[fromID].roles.includes('99565011696910336')) ? true : false;
	//console.log(txt,raw);

	// Unmute from Emergency Mode
	if ( text === "!unmute" && (fromAdmin || fromMod) ) {
		botSay('Emergency Mute Mode Deactivated!',to);
		mute = false;
	}

	// Dance Detector
	if (text.includes('o') || text.includes('0') && fromID === "149541152322879489" ) {
		if ( text.includes('/') || text.includes('\\') || text.includes('<') || text.includes('>') ) {
			console.log('Dancer Detected');
			dance = msgID;
		}
	}

	// Abort if bot is saying something / Don't respond to non-#community chat
	if (mute === true || spammer[fromID] === true) return false;

	// If from Mastabot, check for timed message otherwise ignore
	if (fromID === mastabot) {
		if (txt.includes("🕑")) countdownMessage(msgID,txt,to,5);
		return false;
	}

	// Bookbot cleanup
	if (fromID === book && to === chan) {
		if (bookDelete != false && text.length > 240) {
			botDel(msgID,to,100); // Delete Bookbot message
			botDel(bookDelete[0],to,100); // Delete user request
			// Send it to botplayground
			botSay(bookDelete[1], playground);
			// Edit to ping user
			// Add notice in community chat
			botSay(`<@${bookDelete[2]}>, I have moved your info request to the more appropriate <#${playground}>, head on over there to ask Bookbot anything you want! :thumbsup:`, to);
			bookDelete = false;
		} else {
			bookDelete = false;
		}
	}

	// DEBUG
	// if (from != "Mastastealth" ) {
	// 	console.log('Not a Masta command, rejecting.')
	// 	return false;
	// }

	// Say something as Bot
	if (fromID === me && text.includes("!say") ) {
		botSay(txt.replace('!say ',''));
	}

	// Emergency Mute
	if ( text === "!mute" && (fromAdmin || fromMod) ) {
		botSay('Emergency Mute Mode Activated!',to);
		mute = true;
	}

	// SPAM Checker
	if ( text.startsWith("!") && !text.startsWith("!say") ) {
		command.push(fromID); // Add command to array
		let c = getCount(command,fromID);
		if (c===3) {
			let v = [
				`Take it easy on the command spam <@${fromID}> or you'll be in big trouble.`,
				`<@${fromID}> simmer down, OR ELSE.`,
				`<@${fromID}> take a chill pill or I'll make you.`,
				`Calm down <@${fromID}>, too much spam and you're in for it.`
			];

			let n = Math.floor( Math.random()*4 );
			botSay(v[n], to);
		} else if (c>2) {
			botSay(`<@${fromID}>, I'm going to ignore you for the next 2 minutes. Way to go.`,to);
			spammer[fromID] = true;

			setTimeout( function() {
				delete spammer[fromID];
			},120000);
		}
	}

	// Greeting
	if ( text.startsWith("hey") || text.startsWith("yo") || text.startsWith("hello") || text.startsWith("hi") || text.startsWith("o/") || text.startsWith("sup") ) {
		if ( !text.includes("pocketbot") ) return false;

		let v = [
			`Hi. o/`,
			`Ahoy!`,
			`Hola <@${fromID}>.`,
			`Sup?`
		];

		let n = Math.floor( Math.random()*4 );
		botSay(v[n], to);
	}
	// Depart
	if ( text.startsWith("bye") && text.includes("pocketbot")) botSay(`Cya ${from}`, to);

	// Member check
	if ( (fromAdmin || fromMod) && text.startsWith("!check")) {
		console.log(text);
		let u = getUser(text);
		if (!bot.servers[chan].members[u]) {
			botSay(`🕑 Dont recognize member: \`${u}\``, to)
			return false;
		} else {
			botDel(msgID,to,100);
			let online = (bot.servers[chan].members[u].status === "online") ? ":large_blue_circle:" : ':white_circle:';
			let user = bot.servers[chan].members[u].username;
			let nick = (bot.servers[chan].members[u].nick != undefined) ? `\n aka ${bot.servers[chan].members[u].nick}` : '';
			let disc = bot.servers[chan].members[u].discriminator;
			let d = new Date(bot.servers[chan].members[u]['joined_at']);
			let join = `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()} @ ${d.getHours()}:${d.getMinutes()}`
			botSay(`${online} **${user} #${disc}** ${nick}
		**ID:** ${bot.servers[chan].members[u].id}
		**Joined:** ${join}`, fromID);
		}

	}
	//
	// if ( fromAdmin && text.startsWith("!steam")) {
	// 	console.log( steam.getBucketizedData({gameid: ,leaderboardName: }) );
	// }

	if (fromAdmin && text.startsWith("!roletest") ) {
		let u = getUser(text);
		// Remove from new
		botSay(JSON.stringify(bot.servers[chan].roles), to);
		botSay(bot.servers[chan].members[u].roles, to);
	}

	// Bookbot check
	if (text.startsWith('?') && text.length>3) {
		if (bot.servers[chan].members[book].status != "online") {
			if (text.startsWith('?info') || text.startsWith('?unit')) {
				botSay('Bookbot is down. Try `!info <unit>` to get some simple stats.', to);
			} else if (text.startsWith('?guide')) {
				botSay('Sir Glyde has written up some nice guides over at our reddit: https://www.reddit.com/r/ToothAndTail/search?q=Guide+Chapter&restrict_sr=on')
			} else if (text.startsWith('?alpha')) {
				botSay("If you're looking to get into the Tooth and Tail alpha, signup over at: http://toothandtailgame.com/alpha !",to);
			} else if (text.startsWith('?tips')) {
				botSay("Bookbot is dead. My tip: Checkout the highlights on our twitch to see how the pros do it: https://www.twitch.tv/pocketwatch/profile/highlights",to);
			} else if (text.startsWith('?stream')) {
				botSay("Bookbot isn't available right now but try `!stream` to find out when the next stream is!",to);
			} else {
				let v = [
					`My fellow robo-brethren is not here. Try again later.`,
					`Bookbot is sleeping, that lazy bum...`,
					`Dang it Glyde! Bookbot is down!`,
					`Bookbot can be such a poopface sometimes.`
				];

				let n = Math.floor( Math.random()*4 );
				botSay(v[n], to);
			}
		} else { // Online
			// Cleanup community chat
			if (to === chan) {
				if (text.startsWith('?info') || text.startsWith('?counter')) {
					// Delete message ID
					bookDelete = [msgID,text,fromID];
				}
			}
		}

	}

	// Emoji
	if (text.startsWith(":") ) {
		getEmoji(to,text,from,msgID);
	}

	// Named
	if ( text === "mastabot" || text === "@mastabot" ) {
		botSay("Yes? (`!help` will list my commands)", to);
	}

	// Non-command game?
	if ( text.includes("up for a game?") ) {
		if (rdyPpl === 0) {
			botSay("No one is up for a game it seems. If YOU are, mark yourself with '!ready'.", to);
		} else if (rdyPpl === 1) {
			botSay(`Poor <@${players[0]}> is waiting for someone to play with.`, to);
		} else {
			botSay("Oh hey, there are a few people ready for a game: <@"+players.join('>, <@')+">", to);
		}
	}

	// Build
	if ( text.includes("new build live") ) {
		if ( fromAdmin ) {
			let v = [
				":white_check_mark: A new build means it's time to verify! http://toothandtailgame.com/verify",
				":white_check_mark: DO IT. JUST, DO IT: http://toothandtailgame.com/verify",
				":white_check_mark: Don't be a punk, verify that junk: http://toothandtailgame.com/verify",
				":white_check_mark: Verify the game or else you're lame. http://toothandtailgame.com/verify"
			];

			let n = Math.floor( Math.random()*4 );
			botSay(v[n]);
		}
	}

	// Balance
	if ( text === "!balance" || text === "!meta" ) {
		if ( fromAdmin || fromMod ) {
			botSay('http://codepen.io/mastastealth/full/5701b12140c6d7f5bccf3b6a43faee08', fromID);
			botSay(`I've PM'd you a link, ${from}`,to);
		} else {
			let v = [
				"🕑 Give me a moment to pull it up...",
				"🕑 Let's see here...",
				"🕑 `Loading...`",
				"🕑 Retrieving the balance sheet..."
			];

			let n = Math.floor( Math.random()*4 );
			botSay(v[n],to);

			bot.simulateTyping(chan, function() {
				shot('http://codepen.io/mastastealth/full/5701b12140c6d7f5bccf3b6a43faee08/',
				'screen.png', {
					renderDelay: 2000,
					windowSize: { width: 1024 , height: 960 }
				},  function(err) {
					botSend('screen',from,to);
				});
			});
		}
	}

	// Info
	if (text.startsWith('!info')) {
		let item = text.replace('!info ','');
		switch (item) {
			case 'squirrels':
				item = 'squirrel'
				break;
			case 'lizards':
				item = 'lizard'
				break;
			case 'toads':
				item = 'toad'
				break;
			case 'wire':
			case 'bw':
			case 'barbed wire':
			case 'barbwire':
				item = 'barbedwire'
				break;
			case 'mine':
			case 'mines':
				item = 'landmine'
				break;
			case 'cham':
			case 'chams':
				item = 'chameleon'
				break;
			case 'sniper balloon':
			case 'sniperballoon':
			case 'sniper':
			case 'baloon':
				item = 'balloon'
				break;
			case 'turret':
			case 'turrets':
			case 'nest':
			case 'machine gun':
			case 'mg':
			case 'mgn':
				item = 'machinegun'
				break;
		}
		//console.log(u.units[item]);

		if (u.units[item]) {
			let label = (u.units[item].label != undefined) ? u.units[item].label : u.units[item].longname;
			if (label === undefined) label = '';
			let traits = u.units[item].traits;
			let cost = (u.units[item].cost != undefined) ? u.units[item].cost : 'na';
			let range = '-';
			let tier = (u.units[item].tier) ? `[T${Math.ceil(u.units[item].tier)}] ` : '';

			// Weapon Checker
			for (let t in traits) {
				if ( u.filters.traits[ traits[t] ].hasOwnProperty('wpn') ) {
					// Is weapon, get range
					let w = u.filters.traits[ traits[t] ].wpn.replace('weapon_','');
					range = u.weapons[w].AtkRange;
				}
			}

			// Generate the image
			gm(`assets/unit_${item}.png`).background('transparent')
			.resize(50,50).gravity('West').extent(350, 72,'-20-8')
			.font('assets/snow.otf').fill('#CCC').fontSize(15).drawText(75,-10,label)
			.font('assets/prop.ttf').fill('#FFFFFF').fontSize(14).drawText(75,7,`${tier}`+u.units[item].name)
			.font('assets/snow.otf').fill('#EDEDED').fontSize(11).drawText(75,24, 'TRAITS: '+traits.join(', ') )
			.fill('#FF4136').fontSize(14).drawText(285,-10,`DPS: ${u.units[item].atk}`)
			.fill('#DD3025').fontSize(12).drawText(285,3,`RANGE ${range}`)
			.fill('#0074D9').fontSize(14).drawText(285,22,`HP: ${u.units[item].def}`)
			.fill('#FF851B ').drawText(27,32,`$: ${cost}`)
			.write('unit.png', function (err) {
				if (err) { console.log(err); } else {
					// Flatten
					gm().command('composite')
					.in('unit.png').in('assets/wood.png')
					.write('unit.png', function (err) {
						botSend('unit',from,to);
					})
				}
			});
		} else if (u.filters.traits[item]) {
			let t = u.filters.traits[item];
			let w = (t.wpn) ? ':gun:' : '';
			let wpn = (t.wpn) ? u.weapons[ t.wpn.replace('weapon_','') ] : '';
			let extra = (w != '') ? `\n \`Cooldown: ${wpn.cool} | Range: ${wpn.AtkRange} | Aggro: ${wpn.AggRange}\`` : '';
			botSay(`${w} **${t.label}** - ${t.desc}${extra}`, to)
		} else {
			botSay("I don't recognize that unit/trait.",to);
			return false;
		}
	}

	// Fight Calculator
	if (text.startsWith('!fight') && fromAdmin) {
		// Cleans string to be array of 2 strings: [# <unit>, # <unit>]
		let fight = text.replace('!fight','').replace(/tnt/g,'').replace('vs.','vs').split('vs');

		if (typeof fight != 'object' || fight.length < 2) {
			botDel(msgID,to,100);
			botSay('🕑 Invalid command. Please use format: `# <unit> vs # <unit>` and use the unit emoji.',to);
			return false;
		}

		let u1 = ( fight[0].match(':(.*?):') ) ? fight[0].match(':(.*?):')[1] : undefined;
		let u2 = ( fight[1].match(':(.*?):') ) ? fight[1].match(':(.*?):')[1] : undefined;

		// Creates objects for each team
		let team1 = {
			unit: u1,
			count: parseInt( fight[0].match(/[0-9]+/) ),
		};
		let team2 = {
			unit: u2,
			count: parseInt( fight[1].match(/[0-9]+/) ),
		};

		// Check legit request
		if (team1.unit === undefined || team2.unit === undefined) {
			botDel(msgID,to,100);
			botSay('🕑 Invalid unit detected. Please use format: `# <unit> vs # <unit>` and use the unit emoji.',to);
			return false;
		}
		if (text.includes('wolf') || text.includes('owl') || text.includes('badger') || text.includes('fox') || text.includes('boar')) {
			botSay('Unsupported unit detected. Stick to Tier 1/2 for now, preferably.',to);
			return false;
		}
		if (Number.isNaN(team1.count) || Number.isNaN(team2.count)) {
			botDel(msgID,to,100);
			botSay('🕑 Invalid unit count detected. Please use format: `# <unit> vs # <unit>` and use a proper number.',to);
			return false;
		}
		if (team1.count < 1 || team2.count < 1) {
			botDel(msgID,to,100);
			botSay("🕑 Invalid unit number detected. You need at least 1 per side.",to);
			return false;
		}
		if (team1.count > 80 || team2.count > 80) {
			botDel(msgID,to,100);
			botSay("🕑 Number too large. TnT can't even handle that!",to);
			return false;
		}

		// Enhance each team with legit unit stats
		team1.atk = (team1.unit != 'pigeon') ? u.units[team1.unit].atk : 0;
		team1.def = u.units[team1.unit].def;
		team2.atk = (team2.unit != 'pigeon') ? u.units[team2.unit].atk : 0;
		team2.def = u.units[team2.unit].def;

		if (team1.atk === 0 && team2.atk === 0) {
			botSay("No one can apparently attack each other.",to);
			return false;
		}

		//console.log(u.units[team1.unit].traits);
		if (u.units[team1.unit].traits.includes('meleemole') ||  u.units[team1.unit].traits.includes('hammer')) {
			if (u.units[team2.unit].traits.includes('flying') && team2.atk === 0) {
				botSay("Melee can't hit the flying unit, while flying unit can't attack.",to);
				return false;
			}
		}

		if (u.units[team2.unit].traits.includes('meleemole') ||  u.units[team2.unit].traits.includes('hammer')) {
			if (u.units[team1.unit].traits.includes('flying') && team1.atk === 0) {
				botSay("Melee can't hit the flying unit, while flying unit can't attack.",to);
				return false;
			}
		}

		// -- add owl/mice combo --

		// Start the calculations!
		let dead = false;
		let t1 = [team1.atk*team1.count, team1.def*team1.count, team1.count];
		let t2 = [team2.atk*team2.count, team2.def*team2.count, team2.count];

		function readjustCounts() {
			// Readjust unit counts
			t1[2] = Math.ceil(t1[1] / team1.def);
			t2[2] = Math.ceil(t2[1] / team2.def);
			// Readjust attack damage
			t1[0] = t1[2] * team1.atk;
			t2[0] = t2[2] * team2.atk;
		}

		while (!dead) {
			// Take damage
			for (let i=0; i<t1[2]; i++) {
				if (team1.def >= team2.atk) t1[1] -= team2.atk; // Unit HP - ATK
				if (team1.def < team2.atk) t1[1] -= team1.def; // but account for overkill
				if (team2.unit === 'toad' || team2.unit === 'mine') {
					t1[1] -= (team2.atk/4); // Assume AOE hits 1 other unit
					t2[1] -= team2.def; // Suicide kill
					readjustCounts();
					console.log(`SUICIDE: ${t2[2]} ${team2.unit}s left. ${t2[1]} HP total, ${t2[0]} ATK`);
					if (t1[1] <= 0 || t2[1] <= 0) dead = true;
				}
				console.log(`${t1[2]} ${team1.unit}s left. ${t1[1]} HP total, ${t1[0]} ATK`);
			}

			for (let i=0; i<t1[2]; i++) {
				if (team2.def >= team1.atk) t2[1] -= team1.atk;
				if (team2.def < team1.atk) t2[1] -= team2.def;
				if (team1.unit === 'toad' || team1.unit === 'mine') {
					t2[1] -= (team1.atk/4);
					t1[1] -= team1.def;
					readjustCounts();
					console.log(`SUICIDE: ${t1[2]} ${team1.unit}s left. ${t1[1]} HP total, ${t1[0]} ATK`);
					if (t1[1] <= 0 || t2[1] <= 0) dead = true;
				}
				console.log(`${t2[2]} ${team2.unit}s left. ${t2[1]} HP total, ${t2[0]} ATK`);
			}

			if (dead) break;
			readjustCounts();
			// Log it
			console.log(t1,t2);
			// If someone's HP total is below 0, finish
			if (t1[1] <= 0 || t2[1] <= 0) dead = true;
		}

		if (t1[1] <= 0 && t2[1] > 0) {
			botSay(`Team with ${team2.unit} wins with ${t2[1]} total HP remaining.`,to);
		} else if (t2[1] <= 0 && t1[1] > 0) {
			botSay(`Team with ${team1.unit} wins with ${t1[1]} total HP remaining.`,to);
		} else {
			botSay('They both died.',to);
		}
	}
	// Keys and junk
	if ( text.includes("alpha key") && text.includes("?") ) {
		botSay(`Learn more about the alpha in the <#${rules}> channel. :thumbsup:`,to);
	}

	function giveKey(lucky,key,mod=false) {
		let whodidthat = (mod) ? "the Moderators' vote" : 'one of the Devs';
		// Direct message
		botSay(`Hey <@${lucky}>, looks like you've been bestowed a key by ${whodidthat}! :key: \n
		Your Tooth and Tail steam key is: \`${key}\`! We hope you enjoy. :smile: Click here to activate it on steam: steam://open/activateproduct \n
		Please make sure to give us feedback in chat! You can mark yourself with \`!ready\`
		to let fellow players know you're up for a game in the #community channel.`,lucky);

		console.log(`<@${lucky}> received ${key}`);

		// Completion message
		botSay(`:tada: <@${lucky}> has now joined the alpha. :tada:`);
		regPlayer(lucky,"");

		// Remove from new
		bot.removeFromRole({
			serverID: chan,
			userID: lucky,
			roleID: noob
		}, function(err,resp) {
			console.log(err,resp);

			// Add to members
			bot.addToRole({
				serverID: chan,
				userID: lucky,
				roleID: member
			});
		});
	}

	if (text.startsWith("!key")) {
		let lucky = getUser(text);
		let memsnap = bot.servers[chan].members;
		console.log(`${from} is attempting to key userID: ${lucky}.\n
Server user: ${bot.servers[chan].members[lucky]} \n
Memsnap: ${memsnap[lucky]}`);

		// Grab last key in the list
		keys.orderByKey().limitToLast(1).once("value", function(snapshot) {
			let k = snapshot.key;
			let kk = snapshot.val();
			console.log(`${from} voted!`);
			console.log(memsnap[lucky]);
			//console.log(k,kk,lucky);

			if (!fromMod && !fromAdmin) {
				botDel(msgID,to,100);
				let v = [
					"🕑 Nice try sneaky pants.",
					"🕑 Nope.",
					"🕑 You are neither an admin, nor mod, be gone, peasant.`",
					"🕑 I'll just ignore that..."
				];

				let n = Math.floor( Math.random()*4 );
				botSay(v[n],to);
				return false;
			}

			// Break if no keys left
			if (!kk) {
				botDel(msgID,to,100);
				botSay(`🕑 Welp. This is awkward <@${fromID}>. We need to refill the key list. Sorry <@${lucky}>, please standby!`,to);
				return false;
			}

			// Check @user
			if (!memsnap[lucky]) {
				botDel(msgID,to,100);
				botSay("🕑 Hmm, that user doesn't exist. Did you @ them correctly?",to);
				return false;
			}

			// Already key'd?
			if ( memsnap[lucky].roles.includes(member) ) {
				botDel(msgID,to,100);
				botSay("🕑 This user is a member and should have a :key: already!",to);
				return false;
			}

			// Mods Vote
			if (fromMod) {
				let votes = 0;
				// Register player and add vote
				soldiers.once("value", function(snap) {
					botDel(msgID,to,100);
					let newplayer = soldiers.child(lucky);
					let x = snap.val()[lucky];
					let voter = {};
					voter[fromID] = true;

					if (!x) {
						console.log("user hasn't received vote before, adding 1 now.");
						newplayer.set( memsnap[lucky] ); // Set once if doesn't exist
						newplayer.child('vote').update(voter);
						votes = 1;
					} else {
						// Check for double vote
						console.log("Checking votes");

						if ( x.hasOwnProperty('vote') ) { // If votes even exist
							console.log("Checking double vote");
							if (x.vote.hasOwnProperty(fromID) ) {
								botSay('🕑 You already voted for them.',fromID)
								return false;
							} else {
								console.log('Has vote and not double');
								newplayer.child('vote').update(voter);
								votes = Object.keys( snap.val()[lucky].vote ).length+1
							}
						} else {
							console.log('Doesnt have votes, adding');
							newplayer.child('vote').update(voter);
							votes = 1;
						}
					}

					botSay(`:key: ${from} has voted to key ${memsnap[lucky].username}, ${votes} of 4!`, history)
					if (votes === 4)  {
						botSay(`:tada: <@${lucky}> has been voted to receive a key.`,history);
						newplayer.set( memsnap[lucky] );
						for (let code in kk) {
							// Removes key from Firebase
							keys.child(code).set({});
							giveKey(lucky,kk[code].key,true);
						}
					}
				});

				return false;
			}

			// Actual Key Giving
			for (let code in kk) {
				// Removes key from Firebase
				keys.child(code).set({});
				giveKey(lucky,kk[code].key);
			}
		});
	}

	// Waifu
	if (text.startsWith("!ding")) {
		botSay("Dong!",to);
	}

	if (text.startsWith("!poop")) {
		if (fromMod || fromAdmin) botSay(":poop:",to);
	}

	// Dance Killer
	if (text === ("!killdancer")) {
		if (!fromAdmin && !fromMod) return false;
		if (dance != "") {
			botDel(msgID,to,100);
			botDel(dance);
			dance = "";
			let weapon = '';
			let x = Math.floor(Math.random() * (4 - 1)) + 1;
			switch (x) {
				case 1:
					weapon = ":knife::astonished:";
					break;
				case 2:
					weapon = ":scream::gun:";
					break;
				case 3:
					weapon = ":dagger::astonished: ";
					break;
				case 4:
					weapon = ":bomb: :skull: ";
					break;
			}

			botSay(`${weapon}`);
		}
	}

	// Battle Systems
	if (text.startsWith("!decrown")) {
		if (fromMod || fromAdmin) {
			console.log('Decrowning...');
			let k = getUser(text);
			if (k != undefined) {
				botDel(msgID,to,100);
				if (!bot.servers[chan].members[k].roles.includes(king)) {
					botSay(`🕑 <@${k}> is just a peasant it seems.`,to);
					return false;
				}
				// Remove from king
				bot.removeFromRole({
					serverID: chan,
					userID: k,
					roleID: king
				}, function(err,resp) {
					console.log(err,resp);
					botSay(`The <@&${king}> has been taken!`,to);
				});
			} else {
				botSay("🕑 Hmm, that user doesn't exist. Did you @ them correctly?",to);
			}
		}
	}

	if (text.startsWith("!crown")) {
		if (fromMod || fromAdmin) {
			console.log('Kinging...');
			let k = getUser(text);
			if (k != undefined) {
				botDel(msgID,to,100);
				if (bot.servers[chan].members[k].roles.includes(king)) {
					botSay(`🕑 <@${k}> already has the Crown!`,to);
					return false;
				}
				bot.addToRole({
					serverID: chan,
					userID: k,
					roleID: king
				}, function(err,resp) {
					console.log(err,resp);
					botSay(`:crown: <@${k}> has obtained the Crown!`,to);
				});
			} else {
				botSay("🕑 Hmm, that user doesn't exist. Did you @ them correctly?",to);
			}
		}
	}

	// Now check commands
	for (let i of commands) {
		//console.log(i);
		if ( text.startsWith(i) ) {
			switch (i) {
				case "!alpha" :
					botDel(msgID,to,100);
					botSay(`:pencil: Learn more about the alpha in the <#${rules}> channel. :thumbsup:`,to);
					break;
				// Matchmaking
				case "!ready":
					regPlayer(fromID,'');
					botDel(msgID,to,100);
					if (!bot.servers[chan].members[fromID].roles.includes(member) && !fromMod && !fromAdmin) {
						botSay(`Sorry ${from}. You'll need the game first!`);
						return false;
					}
					if (!players.includes(fromID)) {
						console.log(from+" is ready");
						if (rdyPpl === 0) {
							botSay(`:ok_hand: Awesome ${from}. I'll ping you when someone wants to play.`);
						} else if (rdyPpl === 1) {
							botSay(`Sweet. Hey <@${players[0]}>, <@${fromID}> wants to play. FIGHT! :crossed_swords:  Click to launch TnT: http://www.toothandtailgame.com/play`);
						} else {
							botSay(`:ok_hand: Nice <@${fromID}>. There are a few people <@&${lfg}>, let's see who will play you...`);
						}
						players.push(fromID);
						rdyPpl = players.length;
						bot.addToRole({
							serverID: chan,
							userID: fromID,
							roleID: lfg
						});
					} else if (players.includes(fromID) != false) {
						let v = [
							`Yea ${from}, I'm aware you're ready. Chill, I'll let you know when someone else is. :stuck_out_tongue: `,
							`Hold your ponies ${from}, I know you're ready, let's wait for someone else. :thinking: `,
							`I know ${from}. Waiting for someone else to play...`,
							`Yea yea, now wait for someone else who wants to play, ${from}! :stuck_out_tongue: `,
							`You were already ready ${from}, but thanks for telling us. Again. :stuck_out_tongue_winking_eye: `
						];

						let n = Math.floor( Math.random()*4 );
						botSay(v[n]);
					}

					break;
				case "!unready":
					botDel(msgID,to,100);
					if (players.includes(fromID)) {
						botSay(`🕑 Okee dokes ${from}. Unmarking you from the list. :+1:`);
						let rem = players.indexOf(fromID);
						if(i != -1) {
							players.splice(rem, 1);
						}
						rdyPpl = players.length;
						bot.removeFromRole({
							serverID: chan,
							userID: fromID,
							roleID: lfg
						});
					} else {
						botSay(`🕑 Uh, you were never ready to begin with ${from}, but thanks for letting us know.`);
					}
					break;
				case "!matches":
				case "!games":
					if (rdyPpl === 0) {
						botSay("No one is up for a game it seems. If YOU are, mark yourself with **!ready**.",to);
					} else if (rdyPpl === 1) {
						botSay(`Poor <@${players[0]}> is waiting for someone to play with. :slight_frown:`,to);
					} else {
						botSay(`There are a few people <@&${lfg}>, let's see who will play you...`,to);
					}
					break;
				case "!bugs":
					botSay(":beetle: Report bugs here: https://docs.google.com/spreadsheets/d/1H-9P-TeZrQVYrJZBi1-Q8lKMZ-2B2lwPCgh0ujZpEGk/edit",to);
					break;
				case "!wiki":
					botSay("Check out the wiki: http://toothandtailwiki.com/",to)
					break;
				case "!controls":
					botSay(":video_game: These are the current controls: http://www.toothandtailgame.com/images/big/ctrl.png",to);
					break;
				case "!verify":
					botSay(":white_check_mark: Click here to verify your TnT files: http://toothandtailgame.com/verify",to);
					break;
				case "!stream":
					botDel(msgID,to,100);
					wolf.query("time till Friday 5pm EST", function(err, result) {
						let time = result.queryresult.pod[1].subpod[0].plaintext[0];
						botSay(`Check us out on Twitch @ http://www.twitch.tv/Pocketwatch (Fridays @ 5pm EST) \n The next stream will be in \`${time}\``,to);
					});
					break;
				case "!tournament":
					botSay("For Tournament and game info, VODs, and other community events, check out http://www.clashofcomrades.com",to);
					break;
				case "!addquote":
					let q = quotes.length;
					let newquote = {
						id: q,
						user: from,
						quote: txt.replace('!addquote ',''),
						time: Math.round(+new Date()/1000)
					}

					quotes.push(newquote);
					jf.writeFileSync('assets/quotes.json', quotes);
					botSay("Added quote #"+q,to);
					break;

				case "!delquote":
					let qq = parseInt( text.replace('!delquote ','') );
					let y = quotes.length;

					if (Number.isNaN(qq)) {
						botSay("🕑 I need a quote _number_, please.", to);
					} else {
						if (quotes[qq] === undefined) {
							botSay("🕑 You cannot delete quotes that don't exist, scrub.", to);
							return false;
						}
						if (qq > 260) {
							if (qq === y) {
								quotes.splice(qq,1);
							} else {
								quotes[qq] = {};
							}
							botDel(msgID,to,100);
							jf.writeFileSync('assets/quotes.json', quotes);
							botSay(`🕑 Deleted quote #${qq}`, to);
						} else {
							botDel(msgID,to,100);
							botSay("🕑 You cannot delete quotes from the archives. You monster.", to);
							return false;
						}
					}
					break;
				case "!quote":
					let n = parseInt( text.replace('!quote ','') );
					if (Number.isNaN(n)) {
						botSay("🕑 I need a quote _number_, please.", to);
					} else {
						if (quotes[n] && quotes[n].hasOwnProperty('id')) {
							let thequote = quotes[n].quote;
							if (thequote.startsWith('http://') || thequote.startsWith('https://')) {
								botSay("#"+quotes[n].id+" - _Quoted by: "+quotes[n].user+"_ \n"+thequote, to);
							} else {
								botSay("#"+quotes[n].id+" - _Quoted by: "+quotes[n].user+"_ ```"+thequote+"```", to);
							}
						} else {
							botSay("🕑 Quote doesn't exist.", to);
						}
					}
					break;
				case "!randquote":
					let x = Math.floor(Math.random() * (quotes.length - 1)) + 1;
					botDel(msgID,to,100);
					if (quotes[x] && quotes[x].hasOwnProperty('id')) {
						botSay("#"+quotes[x].id+" - _Quoted by: "+quotes[x].user+"_ ```"+quotes[x].quote+"```", to);
					} else {
						botSay("I picked a quote that doesn't exist. Dang it.", to);
					}
					break;
				case "!getquote":
					let s = text.replace('!getquote ','');
					let results = [];
					let rt = 0;

					for (let i=0; i<quotes.length; i++) {
						if (quotes[i] && quotes[i].hasOwnProperty('id')) {
							let q = quotes[i].quote.toLowerCase();
							if ( q.includes(s) ) {
								//console.log( quotes[i].quote )
								results.push(`#${quotes[i].id} - ${quotes[i].quote}`);
								rt++;
							}
						}

						if (i+1 >= quotes.length || results.length === 5) {
							//console.log('Finished search.');
							if (results.length === 1) {
								botSay(`\`\`\` ${results.join('\n\n')} \`\`\``, to);
							} else if (results.length > 1 && results.length < 6) {
								botSay(`First ${results.length} results: \n\`\`\` ${results.join('\n\n')} \`\`\``, to);
							} else {
								botSay("🕑 No quotes found with that word.", to);
							}
							break;
						}
					}

					break;
				// Other commands
				case "!team":
					let chosenteam = text.replace('!team ','');
					let legitteam = true;
					switch (chosenteam) {
						case "red":
						case "commoners":
						case "orange":
							botSay(":meat_on_bone: Welcome to the **Commonfolk** faction!", to);
							break;
						case "blue":
							botSay(":tophat: Welcome to the **Longcoats** faction!", to);
							break;
						case "green":
						case "ksr":
						case "military":
							botSay(":dagger: Welcome to the **KSR** faction!", to);
							break;
						case "yellow":
						case "pink":
						case "purple":
						case "wine":
						case "clergy":
							botSay(":church: Welcome to the **Civilized** faction!", to);
							break;
						case "none":
							break;
						default:
							botSay("Not sure what team you tried to join. Syntax is `!team <color>`.", to);
							legitteam = false;
					}

					// Add new player if not in DB
					if (!legitteam) return false;
					regPlayer(fromID,chosenteam);

					break;
				case "!rank":
					if (text === "!rank") {
						// Get your own rank
						ranks.once("value", function(snap) {
							let s = snap.val();
							let rank;

							if (s.hasOwnProperty(fromID)) {
								if (s[fromID] >= 15) { rank = ":chipmunk:"; }
								else if (s[fromID] >= 10) { rank = ":boar:"; }
								else if (s[fromID] >= 6) { rank = ":wolf:"; }
								else if (s[fromID] >= 3) { rank = ":snake:"; }
								else if (s[fromID] >= 1) { rank = ":mouse:"; }
								else if (s[fromID] === 0) { rank = ":hatching_chick:"; }
								botSay(`Your win streak is currently at **${s[fromID]}** games. **RANK:** ${rank}`,to);
							} else {
								botSay("You don't seem to have a rank. :disappointed: Make sure to choose a `!team` and challenge players with `!vs`!",to);
							}
						});
					} else {
						let p = getUser(text);
						ranks.once("value", function(snap) {
							let s = snap.val();
							let rank;

							if (s.hasOwnProperty(p)) {
								if (s[p] >= 15) { rank = ":fox:"; }
								else if (s[p] >= 10) { rank = ":wolf:"; }
								else if (s[p] >= 6) { rank = ":boar:"; }
								else if (s[p] >= 3) { rank = ":snake:"; }
								else if (s[p] >= 1) { rank = ":mouse:"; }
								else if (s[p] === 0) { rank = ":hatching_chick:"; }
								botSay(`${bot.servers[chan].members[p].username}'s win streak is currently at **${s[p]}** games. **RANK:** ${rank}`,to);
							} else {
								if (p === undefined && text.replace('!rank ','') === 'list') {
									botSay(`The ranks are as followed: \n
	Rank 0: :dove: \n
	Rank 1: :chipmunk: \n
	Rank 2: - :lizard: \n
	Rank 3-4: - :lizard: \n
	Rank 5-6: - :boar: \n
	Rank 7-9: - :eagle: \n
	Rank 10-14: - :fox: \n
	Rank 15+: - :wolf: \n
									`, to);
								} else {
									botSay("That user doesn't seem to have a rank yet. :disappointed:",to);
								}
							}
						});
					}
					break;
				case "!emotes":
					botSay("🕑 Current emotes available: `"+emotes.join(', ')+"`",to);
					break;
				case "!challenge":
					botDel(msgID,to,100);

					if (challengers.includes(from)) {
						botSay(`🕑 <@${fromID}>, you already challenged for the Crown in the past 24 hours.`,to)
					} else {
						if (bot.servers[chan].members[fromID].roles.includes(king)) {
							botSay(`🕑 You can't challenge yourself, silly.`,to);
							return false;
						}

						botSay(`:crossed_swords: <@${fromID}> has challenged for the <@&${king}>`,to);
						challengers.push(from);
						// 24h timer
						setTimeout(function(){
							let i = challengers.indexOf(from);
							if (i>-1) challengers.splice(i, 1)
						}, 1000*60*60*24);
					}
					break;
				case "!help":
					botDel(msgID,to,100);
					botSay("🕑 Oh you want my commands? Well they would be: `"+commands.join(', ')+"`",to);
					break;
			}
		}
	}
});
