var fs = require('fs'),
	xml2js = require('xml2js'),
	jf = require('jsonfile');

var count = 0;
var walk = function(dir, action, done) {
	var dead = false; // this flag will indicate if an error occured
	var pending = 0; // this flag will store the number of pending async operations

	var fail = function(err) {
		if(!dead) {
			dead = true;
			done(err);
		}
	};

	var checkSuccess = function() {
		//console.log('Pending: '+pending);
		if(!dead && pending == 0) { done(); }
	};

	var performAction = function(file, stat) {
		if(!dead) {
			try { action(file, stat); }
			catch(error) {  fail(error); }
		}
	};

	// this function will recursively explore one directory in the context defined by the variables above
	var dive = function(dir) {
		pending++; count++; // async operation starting after this line
		fs.readdir(dir, function(err, list) {
			if(!dead) { // if we are already dead, we don't do anything
				if (err) { fail(err); }
				else { // iterate over the files
					list.forEach(function(file) {
						if(!dead) { // if we are already dead, we don't do anything
							var path = dir + "/" + file;
							pending++; count++; // async operation starting after this line
							fs.stat(path, function(err, stat) {
								if(!dead) { // if we are already dead, we don't do anything
									if (err) { fail(err);  }
									else {
										if (stat && stat.isDirectory()) { dive(path);  }
										else {  performAction(path, stat); }
										pending--; checkSuccess(); // async operation complete
									}
								}
							});
						}
					});
					pending--; checkSuccess(); // async operation complete
				}
			}
		});
	};

	// start exploration
	dive(dir);
};

var parser = new xml2js.Parser();
var json = {
	"filters" : {
		"traits" : {}
	},
	"units" : {},
	"weapons": {}
}

function xml2json(file,stat) {
	var unitName = file.substr(file.lastIndexOf('/')+1).replace('structure_','').replace('weapon_','').replace('.xml','');

	fs.readFile(file, function(err, data) {
		parser.parseString(data, function (err, result) {
			if (result.ActorData) {
				var ActorData = result.ActorData;
				if (ActorData.HideInEditor || ActorData.IsCommander) {
					count--;
					return false;
				}
				if (!json.units[unitName]) json.units[unitName] = {};
				var unit = json.units[unitName];
				console.log('Reading '+ActorData.DisplayName[0]);

				unit.name = ActorData.DisplayName[0];
				unit.atk = parseInt(ActorData.Damage[0]);
				unit.def = parseInt(ActorData.Health[0]);
				unit.traits = ActorData.Traits[0].Trait.filter(Boolean);
				unit.formation = parseInt(ActorData.FormationPriority[0]);
			} else if (result.StructureData) {
				if (!result.StructureData.hasOwnProperty('CanBeInDeck')) {
					count--;
					return false;
				}

				// Warren
				if (!result.StructureData.BuildCost) {
					console.log('Reading Warren: '+result.StructureData.DisplayName[0]);
					var u = result.StructureData.Behaviors[0].Production[0]['$'].ProductionData;

					if (!json.units[u]) json.units[u] = {};
					var unit = json.units[u];

					unit['label'] = result.StructureData.LongName[0];
					unit['desc'] = result.StructureData.Description[0];
					unit['tier'] = parseInt(result.StructureData.Tier[0]);
					unit['cost'] = 60*( Math.pow(2, unit['tier']-1) );
				} else { // Defense
					var StructData = result.StructureData;
					console.log('Reading '+StructData.DisplayName[0]);
					if (!json.units[unitName]) json.units[unitName] = {};
					var unit = json.units[unitName];
					//console.log(StructData);
					unit.name = StructData.DisplayName[0];
					if (StructData.LongName) unit.longname = StructData.LongName[0];
					if (StructData.Damage) unit.atk = StructData.Damage[0];
					unit.def = (StructData.Health) ? StructData.Health[0] : '--';
					unit.cost = StructData.BuildCost[0];
					unit.tier = parseInt(StructData.BuildCost[0])/60;
					if (StructData.Description) unit.desc = StructData.Description[0];

					if (StructData.Traits) {
						if (StructData.Traits[0] === '') return false;
						var t = StructData.Traits[0].Trait;
						unit['traits'] = t.filter(Boolean);
					}
					unit["tier"] = StructData.BuildCost/60
				}
			} else if (result.TraitData) {
				console.log(unitName);
				json.filters.traits[unitName] = {
					label: result.TraitData.DisplayName[0],
					desc: result.TraitData.Description[0],
				};

				if (result.TraitData.Weapon) json.filters.traits[unitName]['wpn'] = result.TraitData.Weapon[0];
			} else if (result.WeaponData) {
				console.log('Weapon: '+unitName);
				var wData = result.WeaponData;
				//console.log(wData);
				json.weapons[unitName] = {
					cast: parseFloat(wData.CastTime[0]),
					cool: parseFloat(wData.CoolDown[0]),
					AtkRange: parseInt(wData.AttackRange[0]),
					AggRange: parseInt(wData.AggroRange[0])
				};

				if (wData.ClipSize) json.weapons[unitName]['clip'] = wData.ClipSize[0];
				if (wData.ReloadTime) json.weapons[unitName]['reload'] = wData.ReloadTime[0]
			}

			//count--;
			//console.log(count);

			//if (count===0) {
				var file = __dirname + '/units.json';
				jf.writeFileSync(file, json);
				//console.log(json);
			//}
		});
	});
}

function lol() {
	console.log('=====DONE=====');
}

walk(__dirname + '/data', xml2json, lol);
