"use strict"

let gm = require('gm');
let x = ["meleemole","summon",'sniper'];

gm('icons/unit_crow.png').background('transparent')
.resize(50,50).gravity('West').extent(350, 72,'-20-8')
.font('snow.otf').fill('#FFFFFF').fontSize(15).drawText(75,-10,'The Outlaw and the Wretches')
.font('prop.ttf').fill('#EDEDED').fontSize(14).drawText(75,7,`[T3] `+'Barbed Wire')
.font('snow.otf').fill('#CCC').fontSize(11).drawText(75,25,'TRAITS: '+x.join(', '))
.fill('#FF4136').fontSize(14).drawText(285,-10,'DPS: 99')
.fill('#DD3025').fontSize(12).drawText(285,3,'RANGE 9')
.fill('#0074D9').fontSize(14).drawText(285,22,'HP: 999')
.fill('#FF851B ').drawText(27,32,'$: 999')
.write('bla.png', function (err) {
    gm().command('composite')
    .in('bla.png').in('wood.png')
    .write('bla.png', function (err) {
        console.log('yay');
    })
});
