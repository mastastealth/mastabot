let fs = require('fs'),
	flow = require('xml-flow'),
	j2c = require('json2csv');

// Recursive walking junk
let pending;
let walk = function(dir, action, done) {
	var dead = false; // this flag will indicate if an error occured
	pending = 0; // this flag will store the number of pending async operations

	var fail = function(err) {
		if(!dead) {
			dead = true;
			done(err);
		}
	};

	var checkSuccess = function() {
		//console.log('Pending: '+pending);
		if(!dead && pending == 0) { done(); }
	};

	var performAction = function(file, stat) {
		if(!dead) {
			try { action(file, stat); }
			catch(error) {  fail(error); }
		}
	};

	// this function will recursively explore one directory in the context defined by the variables above
	var dive = function(dir) {
		pending++; // async operation starting after this line
		fs.readdir(dir, function(err, list) {
			if(!dead) { // if we are already dead, we don't do anything
				if (err) { fail(err); }
				else { // iterate over the files
					list.forEach(function(file) {
						if(!dead) { // if we are already dead, we don't do anything
							var path = dir + "/" + file;
							pending++; // async operation starting after this line
							fs.stat(path, function(err, stat) {
								if(!dead) { // if we are already dead, we don't do anything
									if (err) { fail(err);  }
									else {
										if (stat && stat.isDirectory()) { dive(path);  }
										else {  performAction(path, stat); }
										pending--; checkSuccess(); // async operation complete
									}
								}
							});
						}
					});
					pending--; checkSuccess(); // async operation complete
				}
			}
		});
	};

	// start exploration
	dive(dir);
};

// XML Reader
let fields = ['player','unit1','unit2','unit3','unit4','unit5','unit6','faction'];
let json = [];

function xml2json(file,stat) {
	let xml = fs.createReadStream(file)
	let xmlStr = flow(xml);

	xmlStr.on('tag:player', function(p) {
		json.push({
			'player': p.identity.name,
			'unit1': p.deck.cards['0'].replace('warren_',''),
			'unit2': p.deck.cards['1'].replace('warren_',''),
			'unit3': p.deck.cards['2'].replace('warren_',''),
			'unit4': p.deck.cards['3'].replace('warren_',''),
			'unit5': p.deck.cards['4'].replace('warren_',''),
			'unit6': p.deck.cards['5'].replace('warren_',''),
			'faction': p.faction
		});

		xml.destroy();

		var csv = j2c({ data: json, fields: fields });
		fs.writeFile('file.csv', csv, function(err) {
			if (err) throw err;
			console.log('CSV updated with '+file);
		});
	});

	xmlStr.on('error', function(e) {
		console.log(e);
	});
}

function lol() {
	console.log(`Hunting down player data, un momento...`);
	console.log(`----------------------------------------`);
}

walk(__dirname+'/replays', xml2json, lol);
